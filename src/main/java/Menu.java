import java.util.Scanner;
import java.util.logging.Logger;

public class Menu {
    private static final String CAR = "CAR";
    private static final String SHIP = "SHIP";
    private static final String PLANE = "PLANE";
    private static final String BICYCLE = "BICYCLE";
    private static final String EXIT_1 = "EXIT";
    private static Scanner scan;
    final static Logger logger = Logger.getLogger(String.valueOf(Pojazdy.class));
    static Pojazdy pojazdy = new Pojazdy();

    public static void menu1() {

        scan = new Scanner(System.in);
        boolean exit = false;
        String option;


        do {

            logger.info("\nYOUR POSSIBLE CHOICES:" + "\n1.CAR" + "\n2.SHIP" + "\n3.PLANE" + "\n4.BICYCLE" + "\n5.EXIT");

            logger.info("WHAT YOU WANT TO PICK? (1-5): ");
            option = scan.nextLine();

            switch(option) {

                case CAR:
                    pojazdy.Car();
                    break;

                case SHIP:
                    pojazdy.Ship();
                    break;

                case PLANE:
                    pojazdy.Plane();
                    break;

                case BICYCLE:
                    pojazdy.Bicycle();
                    break;

                case EXIT_1:
                    System.exit(0);
                    break;

                default:
                    logger.info("Choose correct answer!");
            }

        }while (!exit);

    }
}
