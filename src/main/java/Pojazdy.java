import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class Pojazdy {
    final static Logger logger = Logger.getLogger(String.valueOf(Pojazdy.class));


    public static void Car() {
        String producer = "Ferrari";
        String producer_2 = "Porshe";
        int maxSpeed = 300;
        int maxSpeed_2 = 280;
        Map<String, Integer> Car = new HashMap<String, Integer>();
        Car.put(producer, maxSpeed);
        Car.put(producer_2, maxSpeed_2);
        logger.info("Cars: " + Car);
        if(maxSpeed > maxSpeed_2) {
            logger.info(producer +" is the fastest car, it's max speed is: " +maxSpeed + " km/h");
        }
        else if(maxSpeed < maxSpeed_2)
        {
            logger.info(producer_2+" is the fastest car, it's max speed is: " +maxSpeed_2 + " km/h");
        }
        else if (maxSpeed == maxSpeed_2)
        {
            logger.info("Cars move at the same speed");
        }

    }

    public static void Ship() {
        String producer = "Airmech";
        String producer_2 = "An-Elec";
        int maxSpeed = 60;
        int maxSpeed_2 = 85;
        Map<String, Integer> Ship = new HashMap<String, Integer>();
        Ship.put(producer, maxSpeed);
        Ship.put(producer_2, maxSpeed_2);
        logger.info("Ships: " + Ship);
        if(maxSpeed > maxSpeed_2) {
            logger.info(producer+" is the fastest ship, it's max speed is:" +maxSpeed + " km/h");
        }
        else if(maxSpeed < maxSpeed_2)
        {
            logger.info( producer_2+" is the fastest ship, it's max speed is:" +maxSpeed_2 + " km/h");
        }
        else if (maxSpeed == maxSpeed_2)
        {
            logger.info("Ships move at the same speed");
        }


    }

    public static void Plane() {
        String producer = " McDonnell Douglas DC-10";
        String producer_2 = "Suchoj SuperJet 100";
        int maxSpeed = 700;
        int maxSpeed_2 = 800;
        Map<String, Integer> Plane = new HashMap<String, Integer>();
        Plane.put(producer, maxSpeed);
        Plane.put(producer_2, maxSpeed_2);
        logger.info("Planes: " + Plane);
        if(maxSpeed > maxSpeed_2) {
            logger.info(producer+" is the fastest plane, it's max speed is: " +maxSpeed + " km/h");
        }
        else if(maxSpeed < maxSpeed_2)
        {
            logger.info(producer_2+" is the fastest plane, it's max speed is: " +maxSpeed_2 + " km/h");
        }
        else if (maxSpeed == maxSpeed_2)
        {
            logger.info("Planes move at the same speed");
        }


    }

    public static void Bicycle() {
        String producer = "Giant";
        String producer_2 = "Scott";
        int maxSpeed = 40;
        int maxSpeed_2 = 40;
        Map<String, Integer> Bike = new HashMap<String, Integer>();
        Bike.put(producer, maxSpeed);
        Bike.put(producer_2, maxSpeed_2);
        logger.info("Bike: " + Bike);
        if(maxSpeed > maxSpeed_2) {
            logger.info(producer+" is the fastest bike, it's max speed is: " +maxSpeed + " km/h");
        }
        else if(maxSpeed < maxSpeed_2)
        {
            logger.info(producer_2+" is the fastest bike, it's max speed is: " +maxSpeed_2 + " km/h");
        }
        else if (maxSpeed == maxSpeed_2)
        {
            logger.info("Bikes move at the same speed");
        }


    }


}
